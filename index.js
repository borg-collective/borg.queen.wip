const express = require('express')
const { CloudEvent, HTTPEmitter, HTTPReceiver } = require('cloudevents-sdk')
const PORT = process.env.PORT || 8080
const app = express()
const receiver = new HTTPReceiver()

const welcome = process.env.WELCOME || `🎉 Welcome 😃`
const function_name = process.env.FUNCTION_NAME || `hello`
const function_code = process.env.FUNCTION_CODE 
|| 
`
let ${function_name} = (data) => {
  console.log("😃 received data", data)

  return {
    response: {
      message: "Hey 🖐️"
    }
  }
}
`

// Interesting blog post from Axel Rauschmayer
// https://2ality.com/2014/01/eval.html
const code = new Function(`"use strict"; \n${function_code}\nreturn ${function_name}`)

// receive event
const receive = (cloudEvent, res) => {
  // received data
  let data = cloudEvent.data
  let headers = HTTPEmitter.headers(cloudEvent)
  let functionResult = code()(data)

  let response = functionResult.response ? functionResult.response : { message: "?"}

  res.set(headers)
  res.status(200).send(response)
  // add error management
}

app.use((req, res, next) => {
  let data = ''
  req.setEncoding('utf8')
  req.on('data', chunk => {
    data += chunk
  })
  req.on('end',  _ => {
    req.body = data
    next()
  })
})

app.post('/', (req, res) => {
  try {
    const event = receiver.accept(req.headers, req.body)
    receive(event, res)
  } catch (error) {
    console.error("😡", error)
    res.status(415)
      .header('Content-Type', 'application/json')
      .send(JSON.stringify(error))
  }
})

app.listen(PORT, _ => {
  console.log(`🤖 [Locutus.🚧] is listening on ${PORT}!`)
  console.log(welcome)
})
